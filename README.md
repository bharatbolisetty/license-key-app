# license-key-app

Utility to generate and validate the license key

Generate Key:

run the following command from the project directory

	sh generate-key test-host 2019-04-01

here first argument is the hostname, second argument is expiry date in yyyy-MM-dd format

Validate Key:

run the following command from the project directory

	sh validate EOMUA3N+hG1rxyA7YLxE0A==

here first argument is the license key.
