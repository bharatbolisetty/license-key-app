package constant

import java.text.SimpleDateFormat

object Constants {

  val SECRET = "$EcR37"
  val DATE_FORMAT = "yyyy-MM-dd"
  val dateFormat = new SimpleDateFormat(DATE_FORMAT)

  val KEY_EXPIRED = "Key is expired"
  val INVALID_EXPIRY_DATE = "Invalid key expiry date"
  val VALID_KEY = "Key is valid"
  val INVALID_DOMAIN = "Key not belong to the current system domain"
  val INVALID_KEY = "Provided key is not valid"
}
