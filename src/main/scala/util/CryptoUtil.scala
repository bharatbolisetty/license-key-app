package util

import java.security.MessageDigest
import java.util.Base64

import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

object CryptoUtil {
  def encrypt(key: String, value: String) = {
    val cipher: Cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
    cipher.init(Cipher.ENCRYPT_MODE, keyToSpec(key))
    new String(Base64.getEncoder.encode(cipher.doFinal(value.getBytes("UTF-8"))))
  }

  def decrypt(key: String, encryptedValue: String) = {
    val cipher: Cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING")
    cipher.init(Cipher.DECRYPT_MODE, keyToSpec(key))
    new String(cipher.doFinal(Base64.getDecoder.decode(encryptedValue.getBytes("UTF-8"))))
  }

  def keyToSpec(key: String): SecretKeySpec = {
    var keyBytes: Array[Byte] = (SALT + key).getBytes("UTF-8")
    val sha: MessageDigest = MessageDigest.getInstance("SHA-1")
    keyBytes = sha.digest(keyBytes)
    keyBytes = java.util.Arrays.copyOf(keyBytes, 16)
    new SecretKeySpec(keyBytes, "AES")
  }

  private val SALT: String = "changeme"
}
