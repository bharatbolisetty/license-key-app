package util

import java.net.InetAddress
import java.util.Date

import constant.Constants

import scala.util.{Failure, Success, Try}

/**
  * Utility to generate and validate lincense key
  */
object LicenseKeyUtil {

  /**
    * Generate the license key by combining domain and expiray date with delimiter '/'
    * and encryting the result with AES algorithm
    *
    * @param domain
    * @param expirationDate
    * @return
    */
  def generateKey(domain: String, expirationDate: String): String = {
    val payload = s"$domain/$expirationDate"
    CryptoUtil.encrypt(Constants.SECRET, payload)
  }

  /**
    * Validates given key whether a valid encrypted key
    * and whether it belongs to current system
    * and whether its expired
    * By decrypting the key with AES algorithm(reverse mechanism in generate logic)
    * and splitting the result with delimiter '/'
    *
    *
    * @param key
    * @return
    */
  def validate(key: String) = {
    val payloadTry = Try(CryptoUtil.decrypt(Constants.SECRET, key))
    payloadTry match {
      case Success(payload) => checkDomainAndExpiry(payload)
      case Failure(_) => (Constants.INVALID_KEY, false)
    }
  }

  def checkDomainAndExpiry(payload: String): (String, Boolean) = {
    val hostname = InetAddress.getLocalHost().getHostName()
    val data = payload.split("/")
    if(hostname != data.head){
      (Constants.INVALID_DOMAIN, false)
    } else {
      val dateString = data.last
      val now = new Date();
      Try(Constants.dateFormat.parse(dateString)) match {
        case Success(date) =>
          if(now.before(date)){
            (Constants.VALID_KEY, true)
          } else {
            (Constants.KEY_EXPIRED, false)
          }
        case Failure(e) =>
          (Constants.INVALID_EXPIRY_DATE, false)
      }
    }
  }
}
