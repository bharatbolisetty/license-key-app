import util.LicenseKeyUtil

/**
  * License Key App
  * Expose 2 functions GENERATE, VALIDATE detected based on first arg
  *
  * example:
  * sbt "run GENERATE somedomain somedate"
  */
object LicenseKeyApp extends App{

  if(args != null && args.length > 1){//if first argument is GENERATE then invoke generate key
    if(args(0) == "GENERATE" && args.length > 2){
      val key = LicenseKeyUtil.generateKey(args(1), args(2))
      printMsg(s"Generated key is $key")
    } else if(args(0) == "VALIDATE"){//if first argument is VALIDATE then invoke validate key
      val result = LicenseKeyUtil.validate(args(1))
      printMsg(result._1)
    }
  } else {
    printMsg("Invalid input")
  }

  def printMsg(msg: String){
    println("---------------------------------------------------------------------------------------------------------------------")
    print("                     ")
    println(msg)
    println("---------------------------------------------------------------------------------------------------------------------")
  }
}
