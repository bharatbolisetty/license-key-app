import java.net.InetAddress
import java.time.Duration
import java.util.Date

import constant.Constants
import org.scalatest.FunSuite
import util.LicenseKeyUtil

class LicenceKeyUtilTest extends FunSuite {

  test("Generator should generate key") {
    val key = LicenseKeyUtil.generateKey("test", "201-02-25 00:00:00")
    assert(!key.isEmpty)
  }

  test("Validator should give valid message") {
    val expiryDate = nextMonth()
    val key = LicenseKeyUtil.generateKey(InetAddress.getLocalHost.getHostName, expiryDate)
    val result = LicenseKeyUtil.validate(key)
    assert(result._2)
    assert(result._1 == Constants.VALID_KEY)
  }

  test("Detect expired keys") {
    val expiryDate = yesterday()
    val key = LicenseKeyUtil.generateKey(InetAddress.getLocalHost.getHostName, expiryDate)
    val result = LicenseKeyUtil.validate(key)
    assert(!result._2)
    assert(result._1 == Constants.KEY_EXPIRED)
  }

  test("Detect wrong keys") {
    val result = LicenseKeyUtil.validate("invalid_key")
    assert(result._1 == Constants.INVALID_KEY)
    assert(!result._2)
  }

  test("Detect wrong date") {
    val key = LicenseKeyUtil.generateKey(InetAddress.getLocalHost.getHostName, "invalid_date")
    val result = LicenseKeyUtil.validate(key)
    assert(result._1 == Constants.INVALID_EXPIRY_DATE)
    assert(!result._2)
  }

  test("check domain of current host") {
    val expiryDate = nextMonth()
    val key = LicenseKeyUtil.generateKey("invlaid_host", expiryDate)
    val result = LicenseKeyUtil.validate(key)
    assert(result._1 == Constants.INVALID_DOMAIN)
    assert(!result._2)
  }

  def nextMonth() = {
    val nextMonthMillis = new Date().getTime + Duration.ofDays(30).toMillis
    val futureDate = new Date(nextMonthMillis);
    Constants.dateFormat.format(futureDate)
  }

  def yesterday() = {
    val nextMonthMillis = new Date().getTime - Duration.ofDays(1).toMillis
    val futureDate = new Date(nextMonthMillis);
    Constants.dateFormat.format(futureDate)
  }
}
